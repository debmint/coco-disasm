/* amode.c */
void amode_init(void);
void amode_add_from_string(gchar *ptr);
void amode_list_edit_cb(GtkMenuItem *mi, glbls *fdat);
char *get_addressing_mode(char *str, char *opcode, char *mnemonic, char *oprand);
gchar *guess_addr_mode(gchar *mnem);
GtkWidget *pack_offset_entry(GtkBox *mainbox);
void adr_mode_cb(GtkMenuItem *mi, glbls *fdat);
/* dasmedit.c */
void doc_set_modified(FILEINF *doc, gboolean value);
GtkWidget *bounds_aligned_frame(GtkBox *box, gchar *title);
GtkWidget *build_label_selector(gchar **modept, gboolean with_entry);
void txtbuf_insert_line(GtkTextBuffer *t_buf, GString *g_str);
void abort_warn(char *msg);
GtkWidget *build_dialog_cancel_save(gchar *title, gboolean hide_on_delete);
void sensitize_amode(GtkComboBox *btype, struct adr_widgets *wdgs);
void bnds_define_cb(GtkMenuItem *mi, glbls *fdat);
void rename_label(GtkMenuItem *mi, glbls *fdat);
void lbl_insert_line(GtkMenuItem *mi, glbls *fdat);
void lbl_delete_line(GtkMenuItem *mi, glbls *fdat);
void lbl_properties(GtkMenuItem *mi, glbls *fdat);
/* filestuff.c */
gchar *ending_slash(const gchar *dirnm);
void set_fname(glbls *hbuf, gchar **flag);
gboolean onListRowButtonPress(GtkTreeView *treevue, GdkEventButton *event, GtkMenu *popup);
void set_chooser_folder(GtkFileChooser *chooser, glbls *hbuf);
void selectfile_open(glbls *hbuf, const gchar *type, gboolean IsFile, const gchar *fnam);
void selectfile_save(glbls *hbuf, const gchar *cur_name, const gchar *type);
gint save_all_query(void);
gint save_warn_OW(gchar *type, gchar *fnam, gboolean can_cancel);
void list_store_empty(FILEINF *fdat);
void clear_text_buf(FILEINF *fdat);
void load_text(FILEINF *fdat, GtkWidget *my_win, gchar **newfile);
int str_digit(char *str);
int str_xdigit(char *str);
void load_lbl(FILEINF *fdat, GtkWidget *my_win, gchar **newfile);
void sysfailed(char *msg, gboolean free_it);
void run_disassembler(GtkMenuItem *mi, glbls *hbuf);
void dasm_list_to_file_cb(GtkMenuItem *mi, glbls *hbuf);
void free_filename_to_return(gchar **fname);
void compile_listing(GtkMenuItem *mi, glbls *hbuf);
void load_listing(GtkMenuItem *mi, glbls *hbuf);
void do_cmdfileload(glbls *hbuf);
void load_cmdfile(GtkMenuItem *mi, glbls *hbuf);
void do_lblfileload(glbls *hbuf);
void load_lblfile(GtkMenuItem *action, glbls *hbuf);
void cmd_save_as(GtkMenuItem *mi, glbls *hbuf);
void cmd_save(GtkMenuItem *mi, glbls *hbuf);
void lbl_save_as(GtkMenuItem *mi, glbls *hbuf);
void lbl_save(GtkMenuItem *mi, glbls *hbuf);
void opts_save(GtkMenuItem *mi, glbls *hbuf);
void opts_load(GtkMenuItem *mi, glbls *hbuf);
/* g09dis.c */
gboolean window_quit(void);
GtkWidget *create_main_window(gchar *home);
int main(int argc, char *argv[]);
/* menu.c */
void hlp_about(GtkMenuItem *mi, glbls *hbuf);
void menu_do_dis_sensitize(glbls *hbuf);
void listing_new_cb(GtkMenuItem *mi, glbls *hbuf);
void cmd_new_cb(GtkMenuItem *mi, glbls *hbuf);
void lbl_new_cb(GtkMenuItem *mi, glbls *hbuf);
void tip_toggle(GtkCheckMenuItem *btn, glbls *hbuf);
/* odisopts.c */
GtkWidget *newspin1(int defval);
void set_dis_opts_cb(GtkMenuItem *mi, glbls *hbuf);
/* search.c */
GtkWidget *hsep_show_new(void);
void exact_button_toggled(GtkToggleButton *button, gpointer user_data);
void do_search(GtkWidget *widg, gchar *title, gchar *radioname);
void listing_srch(GtkMenuItem *mi, gpointer user_data);
void labels_srch(GtkMenuItem *mi, gpointer user_data);
